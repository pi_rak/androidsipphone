package com.uksw.pi_rak.androidsipphone;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import javax.sip.InvalidArgumentException;
import javax.sip.ObjectInUseException;
import javax.sip.PeerUnavailableException;
import javax.sip.TransportNotSupportedException;


public class MainActivity extends AppCompatActivity {

    private SipLayer layer;

    private TextView mTextStatus;
    private TextView mTextIp;
    private ScrollView mScrollView;
    private EditText mEditText;
    private TextView mEditTextViewTo;

    final Context that = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTextStatus = findViewById(R.id.TEXT_STATUS_ID);
        mTextIp = findViewById(R.id.MY_IP);
        mEditTextViewTo = findViewById(R.id.TEXT_TO);
        mScrollView = findViewById(R.id.SCROLLER_ID);
        mEditText = findViewById(R.id.EDIT_TEXT_ID);
        String ipAdressWithPort = "sip:" + getLocalIpAddress() + ":1234";
        mEditTextViewTo.setText(ipAdressWithPort);
        mTextIp.setText("YOUR SIP: " + ipAdressWithPort);


        Button buttonSend = findViewById(R.id.BUTTON_SEND);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
                String textNew = mEditText.getText().toString();
                if (!textNew.equals("")) {
                    layer.sendMessageOnExistingDialog(textNew);
                    displayOnChat(textNew);
                } else {
                    Toast.makeText(that, "Write some message.", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button buttonInvite = findViewById(R.id.BUTTON_INVITE);
        buttonInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
                String sipUriRecipient = mEditTextViewTo.getText().toString();
                layer.sendInviteForDialog(sipUriRecipient, "INVITE FOR DIALOG");
            }
        });

        if (!isNetworkAvailable()) {
            showMessage("Network is not available.");
        } else {
            showMessage("Network is OK.");
            new NetworkTask().execute();
        }
    }

    public void performClickOnDone(EditText editView, final View button) {
        mEditTextViewTo.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                hideKeyboard(textView);
                button.requestFocus();
                button.performClick();
                return true;
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public void displayOnChat(final String textNew) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String text = mTextStatus.getText() + "\n" + textNew;

                mTextStatus.setText(text);
                scrollToBottom();
                mEditText.setText("");
            }
        });
    }


    private void scrollToBottom() {
        mScrollView.post(new Runnable() {
            public void run() {
                mScrollView.smoothScrollTo(0, mTextStatus.getBottom());
            }
        });
    }

    public void showMessage(String massage) {
        Snackbar.make(findViewById(android.R.id.content), massage,
                Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    private class NetworkTask extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... params) {
            try {
                String LOCAL_ADDRESS = getLocalIpAddress();
                layer = new SipLayer((MainActivity) that, LOCAL_ADDRESS, 1234);

            } catch (TooManyListenersException | ParseException e) {
                e.printStackTrace();
            } catch (InvalidArgumentException e) {
                e.printStackTrace();
            } catch (PeerUnavailableException e) {
                e.printStackTrace();
            } catch (TransportNotSupportedException e) {
                e.printStackTrace();
            } catch (ObjectInUseException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}