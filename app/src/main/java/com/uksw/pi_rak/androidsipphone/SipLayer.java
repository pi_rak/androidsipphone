package com.uksw.pi_rak.androidsipphone;

import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.TooManyListenersException;

import javax.sip.ClientTransaction;
import javax.sip.Dialog;
import javax.sip.DialogTerminatedEvent;
import javax.sip.IOExceptionEvent;
import javax.sip.InvalidArgumentException;
import javax.sip.ListeningPoint;
import javax.sip.ObjectInUseException;
import javax.sip.PeerUnavailableException;
import javax.sip.RequestEvent;
import javax.sip.ResponseEvent;
import javax.sip.ServerTransaction;
import javax.sip.SipException;
import javax.sip.SipFactory;
import javax.sip.SipListener;
import javax.sip.SipProvider;
import javax.sip.SipStack;
import javax.sip.TimeoutEvent;
import javax.sip.TransactionState;
import javax.sip.TransactionTerminatedEvent;
import javax.sip.TransportNotSupportedException;
import javax.sip.address.Address;
import javax.sip.address.AddressFactory;
import javax.sip.header.CSeqHeader;
import javax.sip.header.CallIdHeader;
import javax.sip.header.ContactHeader;
import javax.sip.header.ContentTypeHeader;
import javax.sip.header.FromHeader;
import javax.sip.header.HeaderFactory;
import javax.sip.header.MaxForwardsHeader;
import javax.sip.header.ToHeader;
import javax.sip.header.ViaHeader;
import javax.sip.message.MessageFactory;
import javax.sip.message.Request;
import javax.sip.message.Response;

//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//import rownolegle.rozproszone.SipGuiController;

// TODO:
// change logger to System.out

public class SipLayer implements SipListener {
    private final static String TAG = SipLayer.class.getName();

    private final Address contactAddress;
    private final ContactHeader contactHeader;

    private Properties properties;

    private SipStack sipStack;

    private SipFactory sipFactory;

    private AddressFactory addressFactory;

    private HeaderFactory headerFactory;

    private MessageFactory messageFactory;

    private SipProvider sipProvider;

    private Dialog dialog;

    private MainActivity guiRef;

    public SipLayer(MainActivity guiRef, String ip, int port) throws
            PeerUnavailableException, InvalidArgumentException, TransportNotSupportedException,
            TooManyListenersException, ObjectInUseException, ParseException {
        this.guiRef = guiRef;

        properties = new Properties();
        properties.setProperty("javax.sip.STACK_NAME", "SipClient");
        properties.setProperty("javax.sip.IP_ADDRESS", ip);

        sipFactory = SipFactory.getInstance();

        sipStack = sipFactory.createSipStack(properties);
        headerFactory = sipFactory.createHeaderFactory();
        messageFactory = sipFactory.createMessageFactory();
        addressFactory = sipFactory.createAddressFactory();

        ListeningPoint udp = sipStack.createListeningPoint(ip, port, "UDP");

        sipProvider = sipStack.createSipProvider(udp);
        sipProvider.addSipListener(this);

        this.contactAddress = this.addressFactory.createAddress("sip:" + getHost() + ":" + getPort());
        this.contactHeader = this.headerFactory.createContactHeader(contactAddress);
        Log.d(TAG, "SIP layer initialized");
    }


    public void processRequest(final RequestEvent requestEvent) {
        Request req = requestEvent.getRequest();

        String method = req.getMethod();
        if (!method.equals(Request.MESSAGE) && !method.equals(Request.REGISTER) && !method.equals(Request.INVITE) && !method.equals(Request.ACK)) {
            Log.e(TAG, "bad request");
            return;
        }

        // TODO: look into this paralelly to processInvite() refactoring.
        if (method.equals(Request.INVITE)) {
            processInvite(requestEvent);
            return;
        }

        if (method.equals(Request.ACK)) {
            guiRef.displayOnChat("Incoming call. Connection established.");
            return;
        }

        FromHeader from = (FromHeader) req.getHeader("From");

//        guiRef.displayOnChat("[" + from.getAddress().toString() + "]: " + new String(req.getRawContent()));
        guiRef.displayOnChat(new String(req.getRawContent()));
        Response response = null;
        try { //Reply with OK
            response = messageFactory.createResponse(200, req);
            ToHeader toHeader = (ToHeader) response.getHeader(ToHeader.NAME);
            toHeader.setTag("1234"); //This is mandatory as per the spec.

            ServerTransaction st;
            if (requestEvent.getServerTransaction() == null) {
                st = sipProvider.getNewServerTransaction(req);
            } else {
                st = requestEvent.getServerTransaction();
            }
            response.addHeader(contactHeader);
            st.sendResponse(response);
            Log.i(TAG, "Sent 200 response. ");
        } catch (Exception e) {
            Log.e(TAG, "Cant send OK signal. Exp:" + e);
        }
    }

    // TODO: this method needs refactoring - be careful. demons inside.
    private void processInvite(final RequestEvent requestEvent) {
        SipProvider sipProvider = (SipProvider) requestEvent.getSource();
        Request request = requestEvent.getRequest();
        try {
            Response response = messageFactory.createResponse(Response.RINGING,
                    request);
            ServerTransaction st = requestEvent.getServerTransaction();

            if (st == null) {
                st = sipProvider.getNewServerTransaction(request);
            }
            dialog = st.getDialog();

            st.sendResponse(response);

            Response okResponse = messageFactory.createResponse(Response.OK,
                    request);

            Address address = this.addressFactory.createAddress("sip:" + getHost() + ":" + getPort());
            ContactHeader contactHeader = headerFactory
                    .createContactHeader(address);
            response.addHeader(contactHeader);
            ToHeader toHeader = (ToHeader) okResponse.getHeader(ToHeader.NAME);
            toHeader.setTag("1234"); // Application is supposed to set.
            okResponse.addHeader(contactHeader);

            ServerTransaction inviteTid = st;
            Request inviteRequest = request;

            Thread.sleep(1000);

            if (inviteTid.getState() != TransactionState.COMPLETED) {
                Log.d(TAG, "shootme: Dialog state before 200: "
                        + inviteTid.getDialog().getState());
                inviteTid.sendResponse(okResponse);
                Log.d(TAG, "shootme: Dialog state after 200: "
                        + inviteTid.getDialog().getState());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception in processInvite method: " + e);
        }
    }

    public void processResponse(final ResponseEvent responseEvent) {
        Response response = responseEvent.getResponse();
        int status = response.getStatusCode();

        Log.i(TAG, "received response: \n\n" + response.toString());

        if ((status >= 200) && (status < 300)) {
            Log.i(TAG, "Message sent properly (received 200 after attempt to send message).");
            Dialog dialog = responseEvent.getClientTransaction().getDialog();
            if (dialog != null) {
                try {
                    Request request = dialog.createAck(((CSeqHeader) response.getHeader("CSeq")).getSeqNumber());
                    Log.d(TAG, "Dialog used to createAck");
                    guiRef.displayOnChat("Your recipient picked up the phone. ");
                    dialog.sendAck(request);
                    Log.d(TAG, "Sent ACK using dialog. ");
                    this.dialog = dialog;
                    Log.d(TAG, "Got non null dialog from responseEvent transaction.  Assigning it to this instance field. ");
                } catch (Exception e) {
                    Log.e(TAG, "Could not send back ACK after receiving 200 when attempt to create dialog. Exception:", e);
                }
            }
            return;
        } else if (status == 180) {
            guiRef.displayOnChat("RINGING... ");
            return;
        }
        guiRef.displayOnChat("Error with sending your message. ");
        Log.e(TAG, "Error sending message. ");
    }

    public void processTimeout(final TimeoutEvent timeoutEvent) {
//        logger.error("time out error. ");
    }

    public void processIOException(final IOExceptionEvent exceptionEvent) {
//        logger.error("IO exception error. ");
    }

    public void processTransactionTerminated(final TransactionTerminatedEvent transactionTerminatedEvent) {
        throw new UnsupportedOperationException();
    }

    public void processDialogTerminated(final DialogTerminatedEvent dialogTerminatedEvent) {
        throw new UnsupportedOperationException();
    }

    public String getHost() {
        return sipStack.getIPAddress();
    }

    public int getPort() {
        return sipProvider.getListeningPoints()[0].getPort();
    }

    public void sendInviteForDialog(final String sipUri, final String plainTextMessage) {
        try {
            Request invite = createMessage(sipUri, plainTextMessage, Request.INVITE);
            Log.d(TAG, "Created INVITE request. ");

            ClientTransaction trans = sipProvider.getNewClientTransaction(invite);
            Log.d(TAG, "Created transaction. ");
            if (dialog == null) {
                dialog = trans.getDialog();
                Log.d(TAG, "Dialog variable was assigned. ");
                trans.sendRequest();
                Log.d(TAG, "INVITE Request sent in created transaction. ");
                guiRef.showMessage("Sent invite for dialog to: " + sipUri);
            } else {
                throw new IllegalStateException("Dialog already open on this instance. ");
            }
        } catch (Exception e) {
            Log.e(TAG, "Error when sending the message inside dialog: " + e.getLocalizedMessage());
            guiRef.showMessage("Error when sending the message inside dialog: " + e.getLocalizedMessage());
        }
    }

    public void sendByeForDialog() {
        try {
            Request request = this.dialog.createRequest(Request.BYE);
            Log.d(TAG, "Created BYE request using Dialog API. ");
            ClientTransaction transaction = this.sipProvider.getNewClientTransaction(request);
            Log.d(TAG, "Created transaction. ");
            this.dialog.sendRequest(transaction);
            Log.d(TAG, "Sent request through dialog. ");
        } catch (SipException e) {
            Log.e(TAG, "Error when sending bye for dialog. " + e);
        }
    }

    /**
     * Create message for given receiver, given content and type.
     *
     * @param sipUri           Where do you want to send the request to?
     * @param plainTextMessage Message to be inserted inside the request. You can access it with request.getContent() on the receiver side.
     * @param messageType      REGISTER, INVITE, BYE, etc. Check SIP protocol specification.
     * @return
     * @throws ParseException
     * @throws InvalidArgumentException
     */
    private Request createMessage(final String sipUri, String plainTextMessage, final String messageType) throws ParseException, InvalidArgumentException {
        // Get the destination address from the text field.
        Address addressTo = this.addressFactory.createAddress(sipUri);
        // Create the request URI for the SIP message.
        javax.sip.address.URI requestURI = addressTo.getURI();

        // Create the SIP message headers.

        // The "Via" headers.
        ArrayList viaHeaders = new ArrayList();
        ViaHeader viaHeader = this.headerFactory.createViaHeader(getHost(), getPort(), "udp", "z9hG4bKbranch1");
        viaHeaders.add(viaHeader);
        // The "Max-Forwards" header.
        MaxForwardsHeader maxForwardsHeader = this.headerFactory.createMaxForwardsHeader(70);
        // The "Call-Id" header.
        CallIdHeader callIdHeader = this.sipProvider.getNewCallId();
        // The "CSeq" header.
        CSeqHeader cSeqHeader = this.headerFactory.createCSeqHeader(1L, messageType);
        // The "From" header.
        FromHeader fromHeader = this.headerFactory.createFromHeader(this.contactAddress, "pi_rak");
        // The "To" header.
        ToHeader toHeader = this.headerFactory.createToHeader(addressTo, null);

        // Create the REGISTER request.
        Request request = this.messageFactory.createRequest(
                requestURI,
                messageType,
                callIdHeader,
                cSeqHeader,
                fromHeader,
                toHeader,
                viaHeaders,
                maxForwardsHeader);
        // Add the "Contact" header to the request.
        request.addHeader(contactHeader);

        ContentTypeHeader contentTypeHeader =
                headerFactory.createContentTypeHeader("text", "plain");
        request.setContent(plainTextMessage, contentTypeHeader);

        return request;
    }

    /**
     * Essentially, you're skipping the "create main elements" step when sending a message inside an existing dialog.
     * When you use an INVITE to create a dialog, don't forget to clean it up by sending an in-dialog BYE message at the end.
     * This technique is also used to refresh registrations and subscriptions.
     *
     * @param plainTextMessage
     */
    public void sendMessageOnExistingDialog(final String plainTextMessage) {
        if (dialog == null) {
            guiRef.displayOnChat("You have no open connections. ");
            Log.w(TAG, "Attempted to send message without existing dialog. ");
            return;
        }

        try {
            Request request = dialog.createRequest(Request.MESSAGE);

            request.setHeader(contactHeader);

            ContentTypeHeader contentTypeHeader =
                    headerFactory.createContentTypeHeader("text", "plain");
            request.setContent(plainTextMessage, contentTypeHeader);

            ClientTransaction trans = sipProvider.getNewClientTransaction(request);
            trans.sendRequest();
        } catch (Exception e) {
            Log.e(TAG, "Error when sending the message inside existing dialog. Exception: " + e.getLocalizedMessage());
        }
    }

}
